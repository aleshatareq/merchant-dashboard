$(window).ready(function () {
	if ($(this).width() <= 991) {
		$('.dashboard-content').css('marginLeft', '0');
		$('.dashboard-menus').css('width', '0');
	}
	else {
		$('.dashboard-content').css('marginLeft', '300px');
		$('.dashboard-menus').css('width', '300px');
	}
});

$(window).resize(function () {
	if ($(this).width() <= 991) {
		$('.dashboard-content').css('marginLeft', '0');
		// $('.dashboard-menus').css('width', '0');
	}
	else {
		$('.dashboard-content').css('marginLeft', '300px');
		$('.dashboard-menus').css('width', '300px');
	}
});


$('.hamburger-menu').on('click', function () {
	var width = $(".dashboard-menus").width();
	var windowLength = $(window).width();
	if (windowLength <= 991) {
		if (width === 300) {
			$('.dashboard-menus').css('width', '0');
			$('.dashboard-content').css('marginLeft', '0');
		}
		else {
			$('.dashboard-menus').css('width', '300px');
			$('.dashboard-content').css('marginLeft', '0');
		}
	}
	else {
		if (width === 250) {
			$('.dash-nav').css('width', '0');
			$('.b2b-dashboard').css('marginLeft', '0');
		}
		else {
			$('.dash-nav').css('width', '250px');
			$('.b2b-dashboard').css('marginLeft', '250px');
		}
	}
})

function imageShow(button) {
	$(button).parent().find('input').click();
}

$(".panel-menu").on("click", function () {
	$(this).toggleClass("active");
});


function readURL(input) {
	if (input.files && input.files[0]) {

		var reader = new FileReader();

		reader.onload = function (e) {
			$(input).parent().hide();
			$(input).parent().siblings('.doc-image-display').show();
			$(input).parent().siblings('.doc-image-display').find('.doc-image').attr('src', e.target.result);
			// $('.image-title').html(input.files[0].name);
		};

		reader.readAsDataURL(input.files[0]);

	} else {
		removeUpload();
	}
}


$('.visit-panel-menu').on('click', function () {
	$(this).toggleClass('active');
})